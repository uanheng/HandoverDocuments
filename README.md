# 交接文档

本人共参与并实现了三个项目,分别为 ROS_CAR,STORM_CAR,EMYOLO.
- ROS_CAR
    + 通过 GPS 获取位置,航向角和速度实现了车的横向及纵向控制.
- STORM_CAR
    + 更改编码风格,从 c 代码风格改为 c++ 风格;
    + 在 ROS_CAR 的基础上添加了雷达的数据处理(基于欧拉核的 K-means++ 聚类算法).
- EMYOLO
    + 标注数据,从拍摄的照片中选择 2000+ 张照片里在选出 700 张目标较为清晰的进行标注;
    + 训练数据,在标注完毕后通过服务器进行训练,训练出 yolov4,yolov4-tiny 的权重;
    + 在代码中通过调用 yolov4 和 MV 的 共享库文件进行图像识别.

## 下载代码

``` bash
# 使用递归下载,以便于下载子模块代码,代码存放至 ~/workspace 目录下
git clone --recurse-submodules https://gitee.com/uanhenguanheng/HandoverDocuments.git ~/workspace
```

## ROS_CAR

### 环境准备
安装 ROS

请参考[官网链接](http://wiki.ros.org/kinetic/Installation/Ubuntu)

下面给出使用中科大镜像源安装方式(具体解释参照官网)：
``` bash
sudo sh -c '. /etc/lsb-release && echo "deb http://mirrors.ustc.edu.cn/ros/ubuntu/ `lsb_release -cs` main" > /etc/apt/sources.list.d/ros-latest.list'

sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

sudo apt-get update

sudo apt-get install ros-kinetic-desktop-full

echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc

sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential

# 添加 ip 地址,可以通过 www.ipaddress.com 查询
sudo sh -c 'echo "199.232.68.133    raw.githubusercontent.com" >> /etc/hosts'

sudo rosdep init

rosdep update
```

安装 ROS 串口库

``` bash
sudo apt install ros-kinetic-serial
```

### 编译工程 && 添加环境变量
``` bash
cd ~/workspace/ROSCAR

catkin_make

echo "source ~/workspace/ROSCAR/devel/setup.bash" >> ~/.bashrc

source ~/.bashrc
```

### 生成注释文档
使用 doxygen 生成注释文档
``` bash
sudo apt install doxygen doxygen-gui graphviz

cd ~/workspace/ROSCAR/src/ROS_CAR/doxygen

doxywizard Doxyfile2.0
```

出现 **doxygen-gui** 界面后,选择 **Run**,再点击 **Run doxygen**,最后点击 **show HTML output**,就可以在网页端看到本程序的解释文档,其中生成的 **HTML** 文件在 **~/ROSCAR/src**的同级 **doc** 目录下.

### 程序结构

如下图所示：

红色字体为话题或者服务名称

![ROSCAR 结构图](./png/roscarstructure.png)

### 使用方法
在使用 **roslaunch** 命令前需添加环境变量.
``` bash
# 启动记录模式,用于保存 gps.txt 文件
roslaunch ros_car ros_recorded.launch
# 启动自动寻迹模式
# 工控机自带 CAN 连接模式
roslaunch ros_car ros_autocar_emuc.launch
# 来可 CAN 连接模式
roslaunch ros_car ros_autocar_lc.launch
```

## STORM_CAR

### 环境准备

参照上文 **ROS_CAR** 的**环境准备**
除此之外,还需要安装 pcap 库(ROS Driver 需要)

``` bash
sudo apt install libpcap-dev
```

### 编译工程 && 添加环境变量
``` bash
cd ~/workspace/STORMCAR

catkin_make

echo "source ~/workspace/STORMCAR/devel/setup.bash" >> ~/.bashrc

source ~/.bashrc
```

### 生成注释文档
使用 doxygen 生成注释文档
``` bash
# 上面已经安装过的可以跳过
sudo apt install doxygen doxygen-gui graphviz

cd ~/workspace/STORMCAR/src/STORM_CAR/Doxygen

doxywizard Doxyfile
```

出现 **doxygen-gui** 界面后,选择 **Run**,再点击 **Run doxygen**,最后点击 **show HTML output**,就可以在网页端看到本程序的解释文档,其中生成的 **HTML** 文件在 **~/STORMCAR/src**的同级 **doc** 目录下.

### 程序结构

如下图所示：

红色字体为话题或者服务名称

![ROSCAR 结构图](./png/stormcarstructure.png)

### 使用方法
在使用 **roslaunch** 命令前需添加环境变量.

``` bash
# 启动记录模式,用于保存 gps.txt 文件
roslaunch storm_car ros_record.launch

# 启动仿真数据模式
roslaunch storm_car ros_map.launch

# 启动点云聚类模式
# 先开启雷达发送点云数据
roslaunch rfans_driver node_manager.launch
# 再开启点云聚类
roslaunch storm_car ros_rasterize.launch

```
本程序尚未完成点云聚类后避障的功能.

## EMYOLO

### 环境准备

#### 安装 OpenCV

``` bash
git clone https://gitee.com/mirrors/opencv ~/workspace/opencv

cd ~/workspace/opencv

git checkout 3.4.11

sudo apt-get install cmake build-essential libgtk2.0-dev libavcodec-dev libavformat-dev libjpeg.dev libtiff4.dev libswscale-dev libjasper-dev

mkdir build && cd build

# 需要联网下载文件,可能会卡一会
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..

make -j16

sudo make install

sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf.d/opencv.conf'

sudo ldconfig
```

#### 安装 CUDA && cuDNN

**CUDA及cuDNN** [百度云链接](https://pan.baidu.com/s/1YG0yvccmqBI2FL2s48Qrxg) **提取码**: **db2u**

网盘文件列表
``` bash
├── 10.0
│   ├── cuda_10.0.130_410.48_linux.run
│   ├── cuda-repo-ubuntu1604-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
│   ├── cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
│   └── cudnn-10.0-linux-x64-v7.6.5.32.tgz
└── 11.0
    ├── cuda_11.0.2_450.51.05_linux.run
    ├── cuda-repo-ubuntu1604-11-0-local_11.0.3-450.51.06-1_amd64.deb
    ├── cuda-repo-ubuntu1804-11-0-local_11.0.3-450.51.06-1_amd64.deb
    ├── cuda-repo-ubuntu2004-11-0-local_11.0.3-450.51.06-1_amd64.deb
    └── cudnn-11.0-linux-x64-v8.0.2.39.tgz
```

CUDA [官网安装](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=2004&target_type=deblocal)方式

下面给出 ubuntu2004 CUDA11 本地 deb 包安装指令(其余请参考官网指导)

``` bash
sudo apt install wget

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin

sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600

# 可从上面的网盘下载 deb 包,而不是通过 wget 指令下载
wget https://developer.download.nvidia.com/compute/cuda/11.0.3/local_installers/cuda-repo-ubuntu2004-11-0-local_11.0.3-450.51.06-1_amd64.deb

sudo dpkg -i cuda-repo-ubuntu2004-11-0-local_11.0.3-450.51.06-1_amd64.deb

sudo apt-key add /var/cuda-repo-ubuntu2004-11-0-local/7fa2af80.pubsudo apt-get update

sudo apt-get -y install cuda

echo 'export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}' >> ~/.bashrc

echo 'export LD_LIBRARY_PATH=/usr/local/cuda/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' >> ~/.bashrc

source ~/.bashrc
```

cuDNN [官网下载链接](https://developer.nvidia.com/rdp/cudnn-download)(需要登陆)
也可从上面网盘下载对应压缩包.

选择 **cuDNN Library for Linux (x86_64)** 下载解压安装

下面给出 cudnn-11.0-linux-x64-v8.0.2.39 的安装方式

``` bash
# 解压 cuDNN
tar zxvf cudnn-11.0-linux-x64-v8.0.2.39.tgz

# 拷贝头文件以及共享库
sudo cp cuda/include/cudnn* /usr/local/cuda/include/
    
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64/

# 给头文件以及共享库添加权限
sudo chmod a+r /usr/local/cuda/include/cudnn*

sudo chmod a+r /usr/local/cuda/lib64/libcudnn*
```

### 编译

#### 准备共享库文件

**相机共享库**
相机的共享库分为 ARM 平台和 x64 平台,需取消 **~/workspace/EMYOLO/Makefile** 文件注释:
``` Makefile
# EM x64
LIBMVSO=./lib/mv/x64/libMVGev.so

# EM arm64
#LIBMVSO=./lib/mv/arm64/libMVGev.so
```
**YOLO 共享库**
yolo 的共享库(libdarknet.so)需要根据平台重新编译,这里的平台包括硬件平台( ARM 和 x86_64),Ubuntu 系统版本,CUDA 版本以及 cuDNN 版本差异.

在编译前需要先安装 OpenCV,CUDA,cuDNN 共享库并修改 **~/workspace/darknet/Makefile** 文件.

修改 Makefile

``` bash
gedit ~/workspace/darknet/Makefile
```
``` makefile
# 将下列选项变更为1
GPU=1
CUDNN=1
CUDNN_HALF=1
OPENCV=1
LIBSO=1
# 根据硬件不同而选择注释不同的 ARCH 选项,这里选择的是 x86_64 平台(显卡为 RTX 2080 Ti 或 RTX 2060)
# GeForce RTX 2080 Ti, RTX 2080, RTX 2070, Quadro RTX 8000, Quadro RTX 6000, Quadro RTX 5000, Tesla T4, XNOR Tensor Cores
ARCH= -gencode arch=compute_75,code=[sm_75,compute_75]

# Jetson XAVIER
# ARCH= -gencode arch=compute_72,code=[sm_72,compute_72]
```

``` bash
# 切换到 darknet 工作空间编译
cd ~/workspace/darknet

make -j16

cd ~/workspace/EMYOLO/ && mkdir -p lib/yolo

cp ~/workspace/darknet/libdarknet.so ~/workspace/EMYOLO/lib/yolo/libdarknet.so
```

#### 编译 EMYOLO

在所有共享库准备完毕之后即可编译
``` bash
cd ~/workspace/EMYOLO

make -j16
```

编译完成后有以下四个可执行文件
``` bash
├── emgetip
├── emsetip
├── emsettempip
└── emyolo
```

### 程序执行

#### 网络地址更改

**相机 IP 地址修改**

``` bash
# 切换到 EMYOLO 工作区
cd ~/workspace/EMYLO/

make clean && make -j16

# 获取相机现有 IP
sudo ./emgetip

# 设置临时 IP 以 192.168.31.200 为例
sudo ./emsettempip 192.168.31.200 255.255.255.0 192.168.31.255

# 设置 IP
sudo ./emsetip 192.168.31.200 255.255.255.0 192.168.31.255

```

**本机 IP 地址修改**

``` bash
# 查看网卡名称 NVIDIA JETSON AGX Xavier 有线网网卡默认为 eth0
ifconfig

sudo ifconfig eth0 192.168.31.100

sudo ifconfig eth0 mtu 9000

```

#### 权重文件下载

[**百度网盘**](https://pan.baidu.com/s/1D0YLcO-1nzE90o164Ejrjw)  **提取码**: **hp8q**

下载后放置在如下两个文件夹
> ~/workspace/EMYOLO/data/weights

> ~/workspace/darknet

#### 开始识别

``` bash
./emyolo
```
**更换网络模型**

本工程支持如下的网络结构：
- yolov4
- yolov4-tiny

由于 NVIDIA JETSON AGX Xavier 的 CPU 性能过于孱弱,本程序默认使用 yolov4-tiny 网络识别目标,可以达到 30fps 左右的帧率(完全版的 yolov4 帧率实测 5fps 左右),可以满足车载端识别要求(10fps).

修改 **yolo_mv.cpp** 文件，将 132~133 行修改为对应 **cfg** 文件和 **weights** 文件并重新编译即可.

网络模型对应文件
- yolov4
    + yolo-obj.cfg
    + yolo-600.weights
- yolov4-tiny
    + yolov4-tiny-obj.cfg
    + yolov4-600-tiny.weights

``` bash
gedit ~/workspace/EMYOLO/src/yolo_mv.cpp
```

修改 132~133 行文件
``` cpp
std::string  cfg_file = "cfg/yolov4-tiny-obj.cfg";
std::string  weights_file = "weights/yolov4-600-tiny.weights";
```

### 标注数据集
本工程使用 [LabelImg](https://gitee.com/circleup/labelImg) 工具标注图像,本工具可以生成 xml 文件或者 txt 文件,其中 txt 文件即为 yolo 训练所需的文件.

#### 安装
Ubunt1604 && Ubuntu1804 : Python 2 + Qt4
``` bash
# 安装 LabelImg
cd ~/workspace/labelImg
sudo apt-get install pyqt4-dev-tools
sudo pip install lxml
make qt4py2
```

Ubuntu2004 : Python 3 + Qt5 (Recommended)
``` bash
# 安装 LabelImg
cd ~/workspace/labelImg
sudo apt-get install pyqt5-dev-tools
sudo pip3 install -r requirements/requirements-linux-python3.txt
make qt5py3
```
#### 使用

``` bash
# 启动 LabelImg
cd ~/workspace/labelImg
python labelImg.py
```

使用方法详见[LabelImg](https://gitee.com/circleup/labelImg) 的 Usage 章节.

### 训练数据集

拍摄用硬件:
- Camera
    + MV-EM200C
- Lens
    + COMPUTAR M1614-MP2

#### 数据集下载
[**数据集**](https://pan.baidu.com/s/145R-_pKTYUGA4NKOdcbzLQ) **提取码**: **cn4d**

#### 构建最小训练工程

参考 [darknet](https://gitee.com/circleup/darknet) 的 **How to train (to detect your custom objects)** 章节.

这里以 600 张图片训练 6个分类的 yolov4-tiny 和 yolov4 为例.

在进行训练必须先编译 darknet 工程,请参考上述 **EMYOLO** 的**环境准备**章节.编译完成后,再执行如下命令.

``` bash
# 导出预训练权重,需要先编译 darknet 工程

cd ~/workspace/darknet

./darknet partial cfg/yolov4-tiny-custom.cfg yolov4-tiny.weights yolov4-tiny.conv.29 29

./darknet partial cfg/yolov4-custom.cfg yolov4.weights yolov4.conv.137 137

# 创建文件夹

cd ~/workspace && mkdir train && cd train 

mkdir backup data weights cfg

# 拷贝可执行文件和数据标签

cp ~/workspace/darknet/darknet ~/workspace/train

cp -r ~/workspace/darknet/data/labels ~/workspace/train/data

# 拷贝导出的预训练权重

cp ~/workspace/darknet/yolov4-tiny.conv.29 ~/workspace/train/

cp ~/workspace/darknet/yolov4.conv.137 ~/workspace/train/
```

train [文件网盘](https://pan.baidu.com/s/17vcIro5VBGHuUrPv-q2VGg) 提取码: 4yp5

从网盘中下载 **.cfg** 文件并放置到 **~/workspace/train/cfg** 文件夹;下载 **.name** 文件以及 **.data** 文件并放置到 **~/workspace/train/data** 文件夹.

train 工程最终列表如下

``` bash
├── cfg
│   ├── yolo-obj.cfg
│   └── yolov4-tiny-obj.cfg
├── darknet
├── data
│   ├── image-100
│   ├── image-600
│   ├── obj.data
│   ├── obj.names
│   └── train.txt
├── yolov4.conv.137
└── yolov4-tiny.conv.29
```

其中 **.cfg**, **.name**, **.data** 文件可参照[darknet](https://gitee.com/circleup/darknet) 的 **How to train (to detect your custom objects)** 章节去修改.

image-600, image-100 需要从网盘下载并解压到 data 文件夹下,train.txt 存放所有图像的路径,解压之后可以从文件夹中找到,将其复制到 data 文件夹下.

#### 开始训练

``` bash
cd ~/workspace/train
# 训练 yolov4-tiny
./darknet detector train data/obj.data cfg/yolov4-tiny-obj.cfg yolov4-tiny.conv.29
# 训练 yolov4
./darknet detector train data/obj.data cfg/yolo-obj.cfg yolov4.conv.137
```

训练后的权重生成在 backup 文件夹下,要使用的话请参考**开始识别**章节的**更换网络模型**去修改成你训练出来的权重(.weights)以及所用的网络模型(.cfg)文件.

## 百度云盘合集
最后给出一个全部文件的合集:

[**链接**](https://pan.baidu.com/s/1G_4cuL85lEmQcioqYBxgmg)

**提取码**: **fxfw**

文件列表:

``` bash
├── cuda
│   ├── 10.0
│   │   ├── cuda_10.0.130_410.48_linux.run
│   │   ├── cuda-repo-ubuntu1604-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
│   │   ├── cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
│   │   └── cudnn-10.0-linux-x64-v7.6.5.32.tgz
│   └── 11.0
│       ├── cuda_11.0.2_450.51.05_linux.run
│       ├── cuda-repo-ubuntu1604-11-0-local_11.0.3-450.51.06-1_amd64.deb
│       ├── cuda-repo-ubuntu1804-11-0-local_11.0.3-450.51.06-1_amd64.deb
│       ├── cuda-repo-ubuntu2004-11-0-local_11.0.3-450.51.06-1_amd64.deb
│       └── cudnn-11.0-linux-x64-v8.0.2.39.tgz
├── data
│   ├── 2007年PASCAL视觉对象类别挑战赛.zip
│   ├── image-100.tar.xz
│   └── image-600.tar.xz
├── train
│   ├── cfg
│   │   ├── yolo-obj.cfg
│   │   └── yolov4-tiny-obj.cfg
│   └── data
│       ├── coco.data
│       ├── coco.names
│       ├── obj.data
│       └── obj.names
└── weights
    ├── yolo-100.weights            # image-100 训练所得权重
    ├── yolo-600.weights            # image-600 训练所得权重
    ├── yolov4-600-tiny.weights     # image-600 训练所得权重
    ├── yolov4-tiny.weights         # 官方权重,coco 数据集
    └── yolov4.weights              # 官方权重,coco 数据集
```